# pypy-artifacts

Repo to store the compressed pypy artifacts.

### Available versions

- [PyPy3.6 v7.3.1](versions/pypy3.6-v7.1.1-linux64.tar.bz2)

- [PyPy3.7 v7.3.4](versions/pypy3.7-v7.3.4-linux64.tar.bz2)